// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "DaedalusCore/Public/WW_BaseSceneComponent_PRNG.h"
#include "PR_SplineWall.generated.h"

/**
 * 
 */
UCLASS()
class DAEDALUSPROCEDURAL_API UPR_SplineWall : public UWW_BaseSceneComponent_PRNG
{
	GENERATED_BODY()
};
