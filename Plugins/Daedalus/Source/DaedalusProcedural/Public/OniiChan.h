// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Perception/AIPerceptionComponent.h"
#include "Perception/AISense_Damage.h"
#include "Perception/AISense_Hearing.h"
#include "Perception/AISense_Sight.h"
#include "Perception/AISense_Team.h"
#include "Perception/AISenseConfig_Damage.h"
#include "Perception/AISenseConfig_Hearing.h"
#include "Perception/AISenseConfig_Sight.h"
#include "Perception/AISenseConfig_Team.h"
#include "OniiChan.generated.h"

UCLASS()
class DAEDALUSPROCEDURAL_API AOniiChan : public AActor
{
	GENERATED_BODY()

public:
	AOniiChan();

	// Called every frame
	virtual void Tick(float DeltaTime) override;
	

	// Okay 
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category="Parameters")
		bool Okay;
	
	// Description not provided  
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Parameters")
		USphereComponent* ExampleSphere;

	// Description not provided  
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Parameters")
		UStaticMeshComponent *MyMeshComp;
	
	
	// SceneComponent to replace RootComponent
	UPROPERTY(EditAnywhere, Category = "Parameters")
		USceneComponent *SceneRoot;
protected:
private:
};


