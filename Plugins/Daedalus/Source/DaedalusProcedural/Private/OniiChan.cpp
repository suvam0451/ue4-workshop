// Fill out your copyright notice in the Description page of Project Settings.

#include "Materials/MaterialInstanceDynamic.h"
#include "Components/StaticMeshComponent.h"
#include "OniiChan.h"

// Sets default values
AOniiChan::AOniiChan()
{
	SceneRoot = CreateDefaultSubobject<USceneComponent>("MyScene");
	SceneRoot->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);

	MyMeshComp = CreateDefaultSubobject<UStaticMeshComponent>("MyStaticMesh");
	MyMeshComp->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
}

void AOniiChan::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);

	UMaterialInterface* mat_0 = MyMesh->GetMaterial(MyMesh);
	UMaterialInstanceDynamic* Inst_mat_0 = UMaterialInstanceDynamic::Create(mat_0, MyMesh); // context
}