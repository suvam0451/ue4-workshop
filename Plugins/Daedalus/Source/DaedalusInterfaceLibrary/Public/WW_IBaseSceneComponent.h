// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "WW_IBaseSceneComponent.generated.h"

//////////////////////////////////////////////////////////////////////
// UWW_IBaseSceneComponent
// Interface for constructing classes that derive from UWW_BaseSceneComponent_PRNG

UINTERFACE()
class DAEDALUSINTERFACELIBRARY_API UWW_IBaseSceneComponent : public UInterface
{
	GENERATED_BODY()
};

class DAEDALUSINTERFACELIBRARY_API IWW_IBaseSceneComponent
{
	GENERATED_BODY()
public:
	// This event is designed to fire when any property is changed in inspector panel
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Overrides")
	void StartDesigner();
};
