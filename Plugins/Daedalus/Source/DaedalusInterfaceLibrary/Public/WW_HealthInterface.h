// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "WW_HealthInterface.generated.h"

//////////////////////////////////////////////////////////////////////
// UWW_HealthInterface

UINTERFACE()
class DAEDALUSINTERFACELIBRARY_API UWW_HealthInterface : public UInterface
{
	GENERATED_BODY()
};

class DAEDALUSINTERFACELIBRARY_API IWW_HealthInterface
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Overrides")
	int ReduceHealth(int Value);
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Overrides")
	int AddHealth(int Value);
};
