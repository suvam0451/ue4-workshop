// Fill out your copyright notice in the Description page of Project Settings.

#include "ActorComp_HP.h"

// Sets default values for this component's properties
UActorComp_HP::UActorComp_HP()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

// Called when the game starts
void UActorComp_HP::BeginPlay()
{
	Super::BeginPlay();

	// ...
}

// Called every frame
void UActorComp_HP::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UActorComp_HP::PostEditChangeProperty(FPropertyChangedEvent &PropertyChangedEvent)
{
	Execute_AddHealth(this, 30);
	Execute_ReduceHealth(this, 20);
}

int UActorComp_HP::ReduceHealth_Implementation(int Value)
{
	UE_LOG(LogTemp, Warning, TEXT("Interface ReduceHealth is not implemented."));
	return 1;
}
int UActorComp_HP::AddHealth_Implementation(int Value)
{
	UE_LOG(LogTemp, Warning, TEXT("Interface AddHealth is not implemented."));
	return 1;
}