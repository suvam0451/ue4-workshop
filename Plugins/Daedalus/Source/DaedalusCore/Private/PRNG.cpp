// Fill out your copyright notice in the Description page of Project Settings.

#include "PRNG.h"

UPRNG::UPRNG()
{
    CurrentSeed = 0;
    RegenerateStream(CurrentSeed);
}

UPRNG::~UPRNG() {}

int UPRNG::GetNextPseudoRandom(int range)
{
    return rnd.RandRange(0, range);
}

void UPRNG::SetSeed(int Value)
{
    rnd = Value;
}

void UPRNG::RegenerateStream(int NewSeed)
{
    rnd = FRandomStream(NewSeed);
}

// void UPRNG::RegenerateStream()
// {
//     int NewSeed = FMath::Rand();
//     rnd = FRandomStream(NewSeed);
// }

int UPRNG::GetNextPseudoRandom(TMap<int, int> WeightedMap)
{
    int total = 0;
    for (auto it : WeightedMap)
    {
        total += it.Value;
    }
    int eval = GetNextPseudoRandom(total);

    for (auto it : WeightedMap)
    {
        eval -= it.Value;
        if (eval <= 0)
        {
            return it.Key;
        }
    }
    return 0;
}