// Fill out your copyright notice in the Description page of Project Settings.

#include "WW_BaseSceneComponent_PRNG.h"

// Sets default values for this component's properties
UWW_BaseSceneComponent_PRNG::UWW_BaseSceneComponent_PRNG()
{
	PrimaryComponentTick.bCanEverTick = true;
	PRNProvider = CreateDefaultSubobject<UPRNG>("Randomness Provider");
}

void UWW_BaseSceneComponent_PRNG::PostEditChangeProperty(FPropertyChangedEvent &PropertyChangedEvent)
{
	if (!LockSeed && GenerateNewSeed)
	{
		CurrentSeed = FMath::Rand();
		GenerateNewSeed = false;
		PRNProvider->RegenerateStream(CurrentSeed);
	}
	Execute_StartDesigner(this);
}

void UWW_BaseSceneComponent_PRNG::StartDesigner_Implementation()
{
	UE_LOG(LogTemp, Warning, TEXT("Interface StartDesigner is not implemented."));
}