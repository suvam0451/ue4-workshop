// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "DaedalusCore.h"

#define LOCTEXT_NAMESPACE "FDaedalusCoreModule"

void FDaedalusCoreModule::StartupModule() {}
void FDaedalusCoreModule::ShutdownModule() {}

#undef LOCTEXT_NAMESPACE

IMPLEMENT_MODULE(FDaedalusCoreModule, DaedalusCore)