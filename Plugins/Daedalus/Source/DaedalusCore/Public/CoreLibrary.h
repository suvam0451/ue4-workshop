// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

/**
 * This is daedalus CoreLibrary which is a dependency of all other Daedalus modules.
 * Must consist of static stateless functions only.
 */
class DAEDALUSCORE_API CoreLibrary
{
public:
	CoreLibrary();
	~CoreLibrary();

	
	/** Searches all Components "type" for given Actor "any" and destroys them */
	template <class any, typename type>
	static void cleanComponentsByType(any* reference) {
		while (reference->GetComponentByClass(type::StaticClass()) != NULL) {
			UActorComponent* cls = reference->GetComponentByClass(type::StaticClass());
			cls->DestroyComponent();
			cls->SetActive(false);
		}
		return;
	}

	/** Takes an array reference and destroys it's components safely. Returns empty array. */
	template <class any>
	static void cleanArray(TArray<any*> &reference) {
		for (int i = 0; i < reference.Num(); i++) {
			if (IsValid(reference[i]) && reference[i] != NULL) {
				reference[i]->Destroy();
			}
		}
		reference.Empty();
	}

	/** Takes a map reference and destroys it's components safely. Returns emprty map. */
	template <class any>
	static void cleanMap(TMap<int, any*> &reference) {
		for (int i = 0; i < reference.Num(); i++) {
			if (IsValid(reference[i]) && reference[i] != NULL) {
				reference[i]->Destroy();
			}
		}
		reference.Empty();
	}
};
