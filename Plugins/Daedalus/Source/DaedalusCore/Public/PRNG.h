// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Kismet/KismetMathLibrary.h"
#include "Components/SplineComponent.h"
#include "PRNG.generated.h"

/**
 * 
 */
UCLASS()
class DAEDALUSCORE_API UPRNG : public UObject
{
	GENERATED_BODY()
public:
	// ctor
	UPRNG();
	~UPRNG();
	// UPRNG(int seed = 0);

	// Accessors
	void SetSeed(int Value);
	int GetSeed() { return CurrentSeed; }

	// Regenerates the generator
	USplineComponent *hello;

	// Regenerates the generator
	void RegenerateStream(int NewSeed);

	// Get the next number in sequence
	int GetNextPseudoRandom(int range);
	// Variant with a Map input
	int GetNextPseudoRandom(TMap<int, int> WeightedMap);

private:
	bool SeedLocked = false;
	int CurrentSeed = 0;
	FRandomStream rnd;
};
