// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Components/SceneComponent.h"
#include "WW_IBaseSceneComponent.h"
#include "PRNG.h"
#include "WW_BaseSceneComponent_PRNG.generated.h"

//////////////////////////////////////////////////////////////////////
// UWW_BaseSceneComponent_PRNG
// (An attachable procedural SceneComponent with a Presudo-Random number generator)
// Derive from both { UWW_BaseSceneComponent_PRNG, IWW_IBaseSceneComponent } to create new class/blueprint.

UCLASS(Blueprintable)
class DAEDALUSCORE_API UWW_BaseSceneComponent_PRNG : public USceneComponent, public IWW_IBaseSceneComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UWW_BaseSceneComponent_PRNG();

	UPROPERTY(VisibleAnywhere, Category = "PRNG")
	int CurrentSeed = 451;
	UPROPERTY(EditAnywhere, Category = "PRNG")
	bool GenerateNewSeed = false;
	UPROPERTY(EditAnywhere, Category = "PRNG")
	bool LockSeed = false;

private:
	// Called when any component property is modified in inspector
	virtual void PostEditChangeProperty(FPropertyChangedEvent &PropertyChangedEvent) override;
	// Dummy SceneComponent. You may use RootComponent instead
	USceneComponent *SceneRoot;

	// Provides seeded random output
	UPRNG *PRNProvider;

	//////////////////////////////////////////////////////////////////////
	// Implementing Interface

public:
	/* Any class inheriting from this base class
	*	must implement the following to get functionalities
	*	Source : IWW_IBaseSceneComponent 
	*/

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Broadcast")
	void StartDesigner();
	virtual void StartDesigner_Implementation() override;
};