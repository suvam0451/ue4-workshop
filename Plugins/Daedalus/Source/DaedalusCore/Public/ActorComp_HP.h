// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "DaedalusInterfaceLibrary/Public/WW_HealthInterface.h"
#include "ActorComp_HP.generated.h"

UCLASS(Blueprintable)
class DAEDALUSCORE_API UActorComp_HP : public UActorComponent, public IWW_HealthInterface
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UActorComp_HP();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction) override;
	// Called when any component property is modified in inspector
	virtual void PostEditChangeProperty(FPropertyChangedEvent &PropertyChangedEvent) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int Health = 0;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Override")
	int ReduceHealth(int Value);
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Override")
	int AddHealth(int Value);
	virtual int ReduceHealth_Implementation(int Value) override;
	virtual int AddHealth_Implementation(int Value) override;
};