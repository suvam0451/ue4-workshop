// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/InstancedStaticMeshComponent.h"
#include "PillarHallActor.generated.h"

UCLASS()
class AMBROSIA_API APillarHallActor : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	APillarHallActor();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// This will be our pillar mesh
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Parameters")
	UStaticMesh *PillarMesh;

	// This will be our floor mesh
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Parameters")
	UStaticMesh *FloorMesh;

	// Instance Static Mesh
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Parameter")
	UInstancedStaticMeshComponent *ISM;
	// Description not provided
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Parameters")
	USceneComponent *SceneRoot;

	// Toggle back to false(when user presses) in OnConstruction to use as a trigger
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Triggers")
	bool Switch = false;

private:
	virtual void OnConstruction(const FTransform &Transform) override;
};
