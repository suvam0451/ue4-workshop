// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/InstancedStaticMeshComponent.h"
#include "RandomizedWallActor.generated.h"

UCLASS()

class AMBROSIA_API ARandomizedWallActor : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ARandomizedWallActor();

public:
	// Map of ISMC
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Parameters")
	TMap<int, UInstancedStaticMeshComponent *> ISMC_Map;

	// This is our map of meshes which would be used in ISMC Map
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Parameters")
	TMap<int, UStaticMesh *> MeshMap;

	// uprop_ismc_map
	// Description not provided
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Parameters")
	TMap<int, UInstancedStaticMeshComponent *> Easy;

	// Designer dictates number of meshes
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Parameters")
	int NumberOfMesh = 0;

	// This acts like a button to change map size
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Parameters")
	bool Switch = false;

	// Updates the map size. TRIES to recover the previous values
	UFUNCTION(BlueprintCallable, Category = "C++")
	void UpdateMapSize();

private:
	// Description not provided
	UPROPERTY(EditAnywhere, Category = "Parameters")
	USceneComponent *SceneRoot;

	virtual void OnConstruction(const FTransform &Transform) override;
};