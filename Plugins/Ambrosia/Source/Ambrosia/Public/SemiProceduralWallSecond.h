// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/InstancedStaticMeshComponent.h"
#include "SemiProceduralWallSecond.generated.h"

UCLASS()
class AMBROSIA_API ASemiProceduralWallSecond : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ASemiProceduralWallSecond();

protected:
public:
	// Description not provided
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Parameters")
	TMap<int, UInstancedStaticMeshComponent *> OniiChan;
};
