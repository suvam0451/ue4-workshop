// Fill out your copyright notice in the Description page of Project Settings.

#include "PillarHallActor.h"

// Sets default values
APillarHallActor::APillarHallActor()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	SceneRoot = CreateDefaultSubobject<USceneComponent>("SceneRoot");
	RootComponent = SceneRoot;
	ISM = CreateDefaultSubobject<UInstancedStaticMeshComponent>("ISM");
	ISM->SetupAttachment(RootComponent); // NOTE: RootComponent ~ ScneRoot
}

// Called when the game starts or when spawned
void APillarHallActor::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void APillarHallActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void APillarHallActor::OnConstruction(const FTransform &Transform)
{
	while (this->GetComponentByClass(UInstancedStaticMeshComponent::StaticClass()) != NULL)
	{
		UActorComponent *cls = this->GetComponentByClass(UInstancedStaticMeshComponent::StaticClass());
		cls->UnregisterComponent();
		cls->DestroyComponent();
		cls->SetActive(false);
	}

	// uinit_ismc to initialize ISMC...
	ISM = NewObject<UInstancedStaticMeshComponent>(this);
	ISM->RegisterComponent();
	ISM->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
	ISM->SetStaticMesh(PillarMesh);

	for (int i = 0; i < 10; i++)
	{
		ISM->AddInstance(FTransform(FVector(100.0f * i, 0, 0)));
	}
	// ISM->AddInstance(FTransform());
	UE_LOG(LogTemp, Warning, TEXT("You have just changed something from inspector."));
}