// Fill out your copyright notice in the Description page of Project Settings.

#include "RandomizedWallActor.h"

// Sets default values
ARandomizedWallActor::ARandomizedWallActor()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	SceneRoot = CreateDefaultSubobject<USceneComponent>("SceneRoot");
	RootComponent = SceneRoot;
}

void ARandomizedWallActor::OnConstruction(const FTransform &Transform)
{
	// This will switch the function we made
	if (Switch == true)
	{
		UpdateMapSize();
		Switch = false;
	}
	// cleanup
	while (this->GetComponentByClass(UInstancedStaticMeshComponent::StaticClass()) != NULL)
	{
		UActorComponent *cls = this->GetComponentByClass(UInstancedStaticMeshComponent::StaticClass());
		cls->UnregisterComponent();
		cls->DestroyComponent();
		cls->SetActive(false);
	}

	// Rest of the code.
	for (int i = 0; i < MeshMap.Num(); i++)
	{
		ISMC_Map.Add(i, NewObject<UInstancedStaticMeshComponent>(this));
		ISMC_Map[i] = NewObject<UInstancedStaticMeshComponent>(this);
		ISMC_Map[i]->RegisterComponent();
		ISMC_Map[i]->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
		ISMC_Map[i]->SetStaticMesh(MeshMap[i]);
	}

	if (MeshMap.Num() >= 2)
	{
		for (int i = 0; i < 10; i++)
		{
			if (i % 2 == 0 && (ISMC_Map[0]->GetStaticMesh() != nullptr))
			{
				ISMC_Map[0]->AddInstance(FTransform(FVector(100.0f * i, 0, 0)));
			}
			else if (ISMC_Map[0]->GetStaticMesh() != nullptr)
			{
				ISMC_Map[1]->AddInstance(FTransform(FVector(100.0f * i, 0, 0)));
			}
		}
	}
}

void ARandomizedWallActor::UpdateMapSize()
{
	// cleanup
	while (this->GetComponentByClass(UInstancedStaticMeshComponent::StaticClass()) != NULL)
	{
		UActorComponent *cls = this->GetComponentByClass(UInstancedStaticMeshComponent::StaticClass());
		cls->UnregisterComponent();
		cls->DestroyComponent();
		cls->SetActive(false);
	}

	TMap<int, UStaticMesh *> TemporaryMeshMap;
	TemporaryMeshMap = MeshMap;

	// We don't need to nuke the references. They haven't used RegisterComponent();
	MeshMap.Empty();
	for (int i = 0; i < NumberOfMesh; i++)
	{
		MeshMap.Add(i, nullptr);
		// MeshMap.Add(i, UStaticMesh)
	}
	// MeshMap.Reserve(NumberOfMesh);

	// Attempt to recover the references.
	for (int i = 0; i < TemporaryMeshMap.Num(); i++)
	{
		if (i < MeshMap.Num())
		{
			MeshMap[i] = TemporaryMeshMap[i];
		}
	}

	// Kill the temp object( This is done automatically. Leaving it here to spread the good habit.)
	TemporaryMeshMap.Empty();
}
