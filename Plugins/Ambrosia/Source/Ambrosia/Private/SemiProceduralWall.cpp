// Fill out your copyright notice in the Description page of Project Settings.

#include "SemiProceduralWall.h"

// Sets default values
ASemiProceduralWall::ASemiProceduralWall()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ASemiProceduralWall::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ASemiProceduralWall::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}
