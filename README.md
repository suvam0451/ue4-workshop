# UE4 test repo

**-- NOTE TO SELF--**

Use this repo for testing new features and idea. Also use this to make transitory changes to main API.

### Work-In-Progress:
- Procedural ISM population.

### Requests
- Wavy flags API along (*ISM with parameter variation* along spline)
- Christmas light (*out-facing shader along spline*/*particles*)
- Wavy water shader

### Features Implemented:
- Spline approximation (Exhaustive seek with steps and iteration).

### API status:
- Currently all API is being reimplemented

Try to keep the size of project small with minimal dependencies.
Keep the online repo junk free.

#### List of things to study and try out
- 
